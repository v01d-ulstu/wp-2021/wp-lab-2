import flask

import config
import views
import api


app = flask.Flask(
    __name__, template_folder=config.TEMPLATE_FOLDER, static_folder=config.STATIC_FOLDER
)
app.register_blueprint(views.views)
app.register_blueprint(api.api, url_prefix="/api/")

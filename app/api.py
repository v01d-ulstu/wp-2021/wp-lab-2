import io

import flask
import weasyprint


api = flask.Blueprint("api", __name__)


@api.route("/make-html")
def api_make_html():
    try:
        image_url = flask.request.args["image_url"]
        text = flask.request.args["text"]
    except KeyError:
        flask.abort(400, "Missing args")

    return flask.render_template("letter.html", image_url=image_url, text=text)


@api.route("/html-to-pdf", methods=["POST"])
def api_html_to_pdf():
    try:
        html = flask.request.get_json()["html"]
    except KeyError:
        flask.abort(400, "Missing args")

    return flask.send_file(
        io.BytesIO(weasyprint.HTML(string=html, encoding="utf-8").write_pdf()),
        mimetype="blob",
    )

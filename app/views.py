import flask


views = flask.Blueprint("views", __name__)


@views.route("/")
def index():
    return flask.render_template("index.html")

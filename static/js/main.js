let result_html = ""


document.querySelector("#make_html_form").onsubmit = async (event) => {
    event.preventDefault()

    const response = await axios.get("/api/make-html", {
        params: {
            image_url: document.querySelector("#image_url").value,
            text: document.querySelector("#text").value
        }
    })

    result_html = response.data

    document.querySelector("#frame_result").src = (
        "data:text/html;charset=utf-8," + result_html
    )
}


document.querySelector("#make_pdf").onclick = async () => {
    if (!result_html) {
        alert("Make HTML first")
        return
    }

    const response = await axios({
        method: "POST",
        url: "/api/html-to-pdf",
        responseType: 'blob',
        data: {
            html: result_html,
        },
    })

    download(response.data, "letter.pdf", "blob")
}
